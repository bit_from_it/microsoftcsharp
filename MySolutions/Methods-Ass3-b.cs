﻿using System;

namespace MethodsAssignmentThree
{
	class Program
	{
		static void Main(string[] args)
		{

			GetStudentInfo ();
			Console.WriteLine();

			GetProfessorInfo ();
			Console.WriteLine();

			GetProgramInfo();
			Console.WriteLine();

			GetCourseInfo();
			Console.WriteLine();

			ValidateStudentBirthday();

		}

		// Getting student info
		static void GetStudentInfo()
		{
			Console.WriteLine ("Enter the student's first name:");
			string StudFirstName = Console.ReadLine ();
			Console.WriteLine ("Enter the student's last name:");
			string StudLastName = Console.ReadLine ();

			Console.WriteLine ("Enter the student's year of birth.");
			string StudBirthYear = Console.ReadLine();

			Console.WriteLine ("Enter the student's month of birth.");			
			string StudBirthMonth = Console.ReadLine();

			Console.WriteLine ("Enter the student's day of birth");			
			string StudBirthDay = Console.ReadLine();

			Console.WriteLine ("Enter the student's Address Line 1");
			string StudAddressLine1 = Console.ReadLine ();

			Console.WriteLine ("Enter the student's Country");
			string StudCountry = Console.ReadLine ();

			// Creating the variable that will hold the information of the student's birthday
			string StudBirthdate = StudBirthDay + "/" + StudBirthMonth + "/" + StudBirthYear;
			
			PrintStudentDetails (StudFirstName, StudLastName, StudBirthdate, StudAddressLine1, StudCountry);
		}
		
		// Print student details function	
		static void PrintStudentDetails(string first, string last, string birthday, string address1, string country)
		{

			Console.WriteLine("{0} {1} was born on: {2}", first, last, birthday);
			Console.WriteLine ("The student lives at {0} and is from {1}", address1, country);
		}

		// Getting professor info
		static void GetProfessorInfo()
		{
			Console.WriteLine ("Enter the professor's first name:");
			string ProfFirstName = Console.ReadLine ();
			Console.WriteLine ("Enter the professor's last name:");
			string ProfLastName = Console.ReadLine ();
			Console.WriteLine ("Enter the professor's basic degree (BSc/BEng/BA)");
			string ProfBSc = Console.ReadLine ();
			Console.WriteLine ("Enter the professor's first graduate degree (MSc/MA)");
			string ProfMSc = Console.ReadLine ();
			Console.WriteLine ("Enter the professor's Phd degree");
			string ProfPhd = Console.ReadLine ();

			PrintProfessorDetails (ProfFirstName, ProfLastName, ProfBSc, ProfMSc, ProfPhd);
		}

		// Print professor details function
		static void PrintProfessorDetails(string first, string last, string bsc, string msc, string phd)
		{

			Console.WriteLine("{0} {1} has the following degrees: {2}, {3}, {4}", first, last, bsc, msc, phd);
		}			

		// Getting program info
		static void GetProgramInfo()
		{
			Console.WriteLine("Enter the University's name:");
			string UnivName = Console.ReadLine();	
			Console.WriteLine ("Enter the school's name:");	
			string SchoolName = Console.ReadLine();
			Console.WriteLine ("Enter the program's name:");
			string ProgramName = Console.ReadLine ();
			Console.WriteLine("Enter the academic year of study:");
			string YearOfStudy = Console.ReadLine();

			PrintProgramDetails (UnivName, SchoolName, ProgramName, YearOfStudy);
		}

		// Print program details function
		static void PrintProgramDetails(string uni, string school, string program, string year)
		{

			Console.WriteLine("You selected to study at the University (of) {0}, in the school (of) {1}, the program {2}, during the academic year {3}.", uni, school, program, year);
		}		

		// Getting Course info
		static void GetCourseInfo()
		{
			Console.WriteLine ("Enter the first course's name:");
			string CourseName1 = Console.ReadLine ();
			Console.WriteLine ("Enter the academic year this course is studied (1,2,3,4)");
			string CourseYear1 = Console.ReadLine ();
			Console.WriteLine ("Enter the second course's name:");
			string CourseName2 = Console.ReadLine ();
			Console.WriteLine ("Enter the academic year this course is studied (1,2,3,4)");
			string CourseYear2 = Console.ReadLine ();
			Console.WriteLine ("Enter the third course's name:");
			string CourseName3 = Console.ReadLine ();
			Console.WriteLine ("Enter the academic year this course is studied (1,2,3,4)");
			string CourseYear3 = Console.ReadLine ();

			PrintCourseDetails (CourseName1, CourseYear1, CourseName2, CourseYear2, CourseName3, CourseYear3);
		}

		// Print course details function
		static void PrintCourseDetails(string name1 , string year1, string name2, string year2, string name3, string year3)
		{

			Console.WriteLine("You have selected the courses: {0}, {2}, {4} of the years {1}, {3}, {5}, respectively", name1, year1, name2, year2, name3, year3);
		}

		static void ValidateStudentBirthday()
		{
			throw new NotImplementedException();
		}
	}
}