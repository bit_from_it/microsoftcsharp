﻿using System;

namespace MethodsAssignmentThree{

	class Program{
		static void Main(string[] args){
			GetStudentInfo ();
			Console.WriteLine();
			GetProfessorInfo ();
			Console.WriteLine();
			//GetProgramInfo();

		}

		// Getting student info
		static void GetStudentInfo(){
			Console.WriteLine ("Enter the student's first name:");
			string StudFirstName = Console.ReadLine ();
			Console.WriteLine ("Enter the student's last name:");
			string StudLastName = Console.ReadLine ();

			Console.WriteLine ("Enter the student's year of birth.");
				string year = Console.ReadLine();
			try{
				 int StudBirthYear  = Int32.Parse(year);
			}
			catch (FormatException e){
				Console.WriteLine("You haven't entered an integer. Please do.");
			}

//			Console.WriteLine ("Enter the student's month of birth.");
//				string StudBirthMonth = Console.ReadLine();
//			try{
//				int month = Int32.Parse(StudBirthMonth);
//			}
//			catch (FormatException e){
//				Console.WriteLine("You haven't entered an integer. Please do.");
//			}
//
//			Console.WriteLine ("Enter the student's day of birth");			
//				string StudBirthDay = Console.ReadLine();
//			try{				
//				int day = Int32.Parse(StudBirthDay);
//			}
//			catch (FormatException e){
//				Console.WriteLine("You haven't entered an integer. Please do.");
//			}

			Console.WriteLine ("Enter the student's Address Line 1");
			string StudAddressLine1 = Console.ReadLine ();

			Console.WriteLine ("Enter the student's Country");
			string StudCountry = Console.ReadLine ();

			// Creating the variable that will hold the information of the student's birthday
			DateTime StudentBirthday = new DateTime (Convert.ToInt32(StudBirthYear), Convert.ToInt32(StudBirthDay), Convert.ToInt32(StudBirthMonth));

			PrintStudentDetails (StudFirstName, StudLastName, StudentBirthday.ToString(), StudAddressLine1, StudCountry);
		}
			
		static void PrintStudentDetails(string first, string last, string birthday, string address1, string country){

			Console.WriteLine("{0} {1} was born on: {2}", first, last, birthday);
			Console.WriteLine ("The student lives at {0} and is from {1}", address1, country);
		}

		// Getting professor info
		static void GetProfessorInfo(){
			Console.WriteLine ("Enter the professor's first name:");
			string ProfFirstName = Console.ReadLine ();
			Console.WriteLine ("Enter the professor's last name:");
			string ProfLastName = Console.ReadLine ();
			Console.WriteLine ("Enter the professor's basic degree (BSc/BEng/BA)");
			string ProfBSc = Console.ReadLine ();
			Console.WriteLine ("Enter the professor's first graduate degree (MSc/MA)");
			string ProfMSc = Console.ReadLine ();
			Console.WriteLine ("Enter the professor's Phd degree");
			string ProfPhd = Console.ReadLine ();

			PrintProfessorDetails (ProfFirstName, ProfLastName, ProfBSc, ProfMSc, ProfPhd);
		}

		static void PrintProfessorDetails(string first, string last, string bsc, string msc, string phd){

			Console.WriteLine("{0} {1} has the following degrees: {2}, {3}, {4}", first, last, bsc, msc, phd);
		}			

		// Getting program info
//		static void GetProgramInfo(){
//			Console.WriteLine ("Enter the course's name:");
//			string CourseName = Console.ReadLine ();
//			Console.WriteLine ("Enter the professor's last name:");
//			string ProfLastName = Console.ReadLine ();
//			Console.WriteLine ("Enter the professor's basic degree (BSc/BEng/BA)");
//			string ProfBSc = Console.ReadLine ();
//			Console.WriteLine ("Enter the professor's first graduate degree (MSc/MA)");
//			string ProfMSc = Console.ReadLine ();
//			Console.WriteLine ("Enter the professor's Phd degree");
//			string ProfPhd = Console.ReadLine ();
//
//			PrintProgramDetails (ProfFirstName, ProfLastName, ProfBSc, ProfMSc, ProfPhd);
//		}
//
//		static void PrintProgramDetails(string first, string last, string bsc, string msc, string phd){
//
//			Console.WriteLine("{0} {1} has the following degrees: {2}, {3}, {4}", first, last, bsc, msc, phd);
//		}		

//			Console.WriteLine("First Name:" + FirstName);
//			Console.WriteLine("Last Name:" + LastName);
//			Console.WriteLine("Birth Data:" + BirthDate);
//			Console.WriteLine("Address Line 1:" + AddressLine1);
//			Console.WriteLine("Address Line 2:" + AddressLine2);
//			Console.WriteLine("City:" + City); 
//			Console.WriteLine("State/Province:" + StateProvince);
//			Console.WriteLine("Zip/Postal:" + ZipPostal);
//			Console.WriteLine("Country:" + Country);
//			Console.WriteLine("Size of Penis in cm:" + SizeOfPenisCm); 
//			Console.WriteLine("Professor Name:" + ProfName);
//			Console.WriteLine("Professor Last Name:" + ProfLast);
//			Console.WriteLine("Degree:" + Degree);
//			Console.WriteLine("University:" + University);
//			Console.WriteLine("UniversityProgram:" + UniversityProgram);
//			Console.WriteLine("Degrees Offered:" + DegreesOffered);
//			Console.WriteLine("DepartmentHead:" + DepartmentHead);
//			Console.WriteLine("Course:" + Course);
	}
}
