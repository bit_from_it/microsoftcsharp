using System;

namespace ConsoleApplication2{
    class Program{
        static void Main(string[] args){
            string oddline = "XOXOXOXO";
            string evenline  = "OXOXOXOX";

	    for (int i = 1; i < 9; i++){
		   if (i%2 == 0){ 
	            	Console.WriteLine(evenline);
		   }
		   else{
			Console.WriteLine(oddline);
		   }
	    }
        }
    }
}
