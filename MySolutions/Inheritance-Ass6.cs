using System;

namespace StructsAssignmentFour
{


	public class Person
	{
		public string FirstName {get; set;}
		public string LastName {get; set;}
	}

	public class Student : Person
	{
		public string BirthDate {get; set;}
		public string AddressLine1 {get; set;}
		public string PostCode {get; set;} 	
		public string Country {get; set;}
		private static int instances = 0;

		public Student(string name, string surname, string birth, string address, string post, string country)
		{
			this.FirstName = name;
			this.LastName = surname;
			this.BirthDate = birth;
			this.AddressLine1 = address;
			this.PostCode = post;
			this.Country = country;

			instances++;
		}

		public void TakeTest()
		{
			Console.WriteLine("Taking a test !");
		}

		public static int GetActiveInstances()
		{
			return instances;
		}
	}

	public class Professor :Person
	{
		public string AddressLine1 {get; set;}
		public string DegreeName {get; set;}
		public string GradDegreeName {get; set;}
		public string PhdName {get; set;}

		public Professor(string name, string surname, string degree, string graddegree, string phd)
		{
			this.FirstName = name;
			this.LastName = surname;
			this.DegreeName = degree;
			this.GradDegreeName = graddegree;
			this.PhdName = phd;
		}

		public void GradeTest()
		{
			Console.WriteLine("Grading a test !");
		}
	}

	public class UniProgram
	{
		public string UniversityName {get; set;}
		public string SchoolName {get; set;}
		public string ProgramName {get; set;}
		public string YearOfStudy {get; set;}
		public string Fees {get; set;}

		public UniProgram(string uname, string sname, string pname, string year, string fees, Degree degree)
		{
			this.UniversityName = uname;
			this.SchoolName = sname;
			this.ProgramName = pname;
			this.YearOfStudy = year;
			this.Fees = fees;
			this.Degree = degree
		}	
	}

	public class Degree
	{
		public string DegreeName {get; set;}
		public string SchoolName {get; set;}
		public string YearOfStudy {get; set;}
		public string Fees {get; set;}

		public Degree(string dname, string sname, string year, string fees, Course course)
		{
			this.DegreeName = dname;
			this.SchoolName = sname;
			this.YearOfStudy = year;
			this.Fees = fees;
			this.Course = course;
		}
	}

	public class Course
	{
			
		public string CourseName {get; set;}
		public string CourseYear {get; set;}
		public string Fees {get; set;}

		private Student[] StudentArray = new Student[3];
		// public Student this [int index]
		// {
		// 	get
		// 	{
		// 		return StudentArray[index];
		// 	}

		// 	set 
		// 	{
		// 		StudentArray[index] = value;
		// 	}
		// }

		private Professor[] ProfessorArray = new Professor[3];
		// public Professor this [int index]
		// {
		// 	get
		// 	{
		// 		return ProfessorArray[index];
		// 	}

		// 	set 
		// 	{
		// 		ProfessorArray[index] = value;	
		// 	}
		// }

		public Course(string name, string year, string fees)
		{
			this.CourseName = name;
			this.CourseYear = year;
			this.Fees = fees;
		}
	}


	class Program
	{
		static void Main(string[] args)
		{
			//Instatiate three student objects
			Student student1 = new Student("Kostas","Prekas","1/1/2000","Ano Kastritsi","26500","Greece");
			Student student2 = new Student("George","Michael","26/6/1963","East Finchley, London","-","United Kingdom");
			Student student3 = new Student("Madonna","Ciccone","16/8/1958","Bay City, MI","-","United States");

			Professor prof1 = new Professor("Christos", "Papadimitriu", "Electrical Engineering Degree NTUA", "Electrical Engineering MSc Princeton", "Electrican Engineering and Computer Science Princeton");

			Course course1 = new Course("Programming with C#","4","Free");

			Degree degree1 = new Degree("Bachelor of Science","School of Science and Technology","2015","Free");

			UniProgram uprogram = new UniProgram("MIT", "Informatics", "Information Technology", "1", "Free");

			//Printing stuff
			// degree1._print();
			Console.WriteLine("The {0} program contains the {1} degree.", uprogram.UniversityName, degree1.DegreeName);
			Console.WriteLine("The {0} degree containes the course {1}.",degree1.DegreeName, course1.CourseName);

			int count = Student.GetActiveInstances();
			Console.WriteLine("There are {0} students in the course.", count);

			Console.WriteLine("I am the professor {0} {1} and I am:", prof1.FirstName, prof1.LastName);
			prof1.GradeTest();
			Console.WriteLine("I am the student {0} {1} and I am:",student1.FirstName, student1.LastName);
			student1.TakeTest();
		}
	}
}