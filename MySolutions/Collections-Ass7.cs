using System;
using System.Collections;

namespace StructsAssignmentFour
{
	public class Person
	{
		public string FirstName {get; set;}
		public string LastName {get; set;}
	}

	public class Student : Person
	{
		public string BirthDate {get; set;}
		public string AddressLine1 {get; set;}
		public string PostCode {get; set;} 	
		public string Country {get; set;}
		private Stack _grades;
		public Stack Grades
			{
		 		get {return _grades;} 
		  		set {_grades = value;} 
		    }
		private static int instances = 0;

		public Student(string name, string surname, string birth, string address, string post, string country)
		{
			this.FirstName = name;
			this.LastName = surname;
			this.BirthDate = birth;
			this.AddressLine1 = address;
			this.PostCode = post;
			this.Country = country;

			instances++;
		}

		public void TakeTest()
		{
			Console.WriteLine("Taking a test !");
		}

		public static int GetActiveInstances()
		{
			return instances;
		}
	}

	public class Professor : Person
	{
		public string AddressLine1 {get; set;}
		public string DegreeName {get; set;}
		public string GradDegreeName {get; set;}
		public string PhdName {get; set;}

		public Professor(string name, string surname, string degree, string graddegree, string phd)
		{
			this.FirstName = name;
			this.LastName = surname;
			this.DegreeName = degree;
			this.GradDegreeName = graddegree;
			this.PhdName = phd;
		}

		public void GradeTest()
		{
			Console.WriteLine("Grading a test !");
		}
	}

	public class UniProgram
	{
		public string UniversityName {get; set;}
		public string SchoolName {get; set;}
		public string ProgramName {get; set;}
		public string YearOfStudy {get; set;}
		public string Fees {get; set;}
		public Degree Degree {get; set;}

		public UniProgram(string uname, string sname, string pname, string year, string fees, Degree degree)
		{
			this.UniversityName = uname;
			this.SchoolName = sname;
			this.ProgramName = pname;
			this.YearOfStudy = year;
			this.Fees = fees;
			this.Degree = degree;
		}	
	}

	public class Degree
	{
		public string DegreeName {get; set;}
		public string SchoolName {get; set;}
		public string YearOfStudy {get; set;}
		public string Fees {get; set;}
		public Course Course {get; set;}

		public Degree(string dname, string sname, string year, string fees, Course course)
		{
			this.DegreeName = dname;
			this.SchoolName = sname;
			this.YearOfStudy = year;
			this.Fees = fees;
			this.Course = course;
		}
	}

	public class Course
	{
			
		public string CourseName {get; set;}
		public string CourseYear {get; set;}
		public string Fees {get; set;}
		public ArrayList Students{get; set;}
		public ArrayList Professors{get; set;} 

		public Course(string name, string year, string fees, ArrayList students, ArrayList professors)
		{
			this.CourseName = name;
			this.CourseYear = year;
			this.Fees = fees;
			this.Students = students;
			this.Professors = professors;
		}

		public void PrintStudents()
		{
			foreach(Student student in Students)
			{	
				Student firstStudent = (Student)Students[0];
				Student secondStudent = (Student)Students[1];
				Student thirdStudent = (Student)Students[2];

				Console.WriteLine(student.FirstName);
				Console.WriteLine(student.LastName);
				Console.WriteLine("");
				
			}
		}
	}


	class Program
	{
		static void Main(string[] args)
		{
			//Instatiate three student objects
			ArrayList studentArray = new ArrayList();
			ArrayList profArray = new ArrayList();

			Student student1 = new Student("Kostas","Prekas","1/1/2000","Ano Kastritsi","26500","Greece");
			Student student2 = new Student("George","Michael","26/6/1963","East Finchley, London","-","United Kingdom");
			Student student3 = new Student("Madonna","Ciccone","16/8/1958","Bay City, MI","-","United States");

			//Adding the students to the studentArray:
			studentArray.Add(student1);
			studentArray.Add(student2);
			studentArray.Add(student3);

			Professor prof1 = new Professor("Christos", "Papadimitriu", "Electrical Engineering Degree NTUA", "Electrical Engineering MSc Princeton", "Electrican Engineering and Computer Science Princeton");

			//Adding the professor(s) to the profArray:
			profArray.Add(prof1);

			//Add grades
			// student1.Grades.Push("5.0");
			// student1.Grades.Push("6.0");
			// student1.Grades.Push("7.0");
			// student1.Grades.Push("8.0");
			// student1.Grades.Push("9.0");
			// student1.Grades.Push("10.0");

			// student2.Grades.Push("5.0");
			// student2.Grades.Push("5.0");
			// student2.Grades.Push("4.0");
			// student2.Grades.Push("4.0");
			// student2.Grades.Push("3.0");
			// student2.Grades.Push("1.0");	

			// student3.Grades.Push("9.0");
			// student3.Grades.Push("9.0");
			// student3.Grades.Push("9.0");
			// student3.Grades.Push("9.0");
			// student3.Grades.Push("9.0");
			// student3.Grades.Push("10.0");		

			Course course1 = new Course("Programming with C#","4","Free", studentArray, profArray);

			Degree degree1 = new Degree("Bachelor of Science","School of Science and Technology","2015","Free", course1);

			UniProgram uprogram = new UniProgram("MIT", "Informatics", "Information Technology", "1", "Free", degree1);

			//Printing stuff
			// degree1._print();
			Console.WriteLine("The {0} program contains the {1} degree.", uprogram.UniversityName, uprogram.Degree.DegreeName);
			Console.WriteLine("The {0} degree containes the course {1}.",degree1.DegreeName, degree1.Course.CourseName);

			int count = Student.GetActiveInstances();
			Console.WriteLine("There are {0} students in the course.", count);

			Console.WriteLine("I am the professor {0} {1} and I am:", prof1.FirstName, prof1.LastName);
			prof1.GradeTest();
			Console.WriteLine("I am the student {0} {1} and I am:",student1.FirstName, student1.LastName);
			student1.TakeTest();
			Console.WriteLine("");

			// Printing student names with foreach loop.
			Console.WriteLine("Printing the names of the students that take the course {0}", course1.CourseName);
			course1.PrintStudents();
		}
	}
}