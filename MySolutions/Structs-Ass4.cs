using System;

namespace StructsAssignmentFour
{

	public struct Student
	{
			public string FirstName;
			public string LastName;
			public DateTime BirthDate;
			public string AddressLine1;
			public string	  PostCode; 	
			public string Country;			
	}

	public struct Professor
	{
			public string FirstName;
			public string LastName;	
			public string AddressLine1;
			public string DegreeName;
			public string GradDegreeName;
			public string PhdName;
	}

	public struct UniProgram
	{
			public string UniversityName;
			public string SchoolName;
			public string ProgramName;
			public string YearOfStudy;
			public string Fees;
	}

	public struct Course
	{
			public string Name;
			public string Year;
			public string Fees;
	}


	class Program
	{
		static void Main(string[] args)
		{

			Student[] arr = new Student[4];	

			// Inserting data
			Console.WriteLine("Please enter the first name of the student");
			arr[0].FirstName = Console.ReadLine();
			Console.WriteLine("Please enter the last name of the student");
			arr[0].LastName = Console.ReadLine();
			Console.WriteLine("Please enter the birthdate of the student in MM/DD/YYYY format");
			arr[0].BirthDate = DateTime.Parse(Console.ReadLine());
			Console.WriteLine("Please enter the address of the student");
			arr[0].AddressLine1 = Console.ReadLine();
			Console.WriteLine("Please enter the postcode of the student");
			arr[0].PostCode = Console.ReadLine(); 
			Console.WriteLine("Please enter the country of origin of the student");
			arr[0].Country = Console.ReadLine();
			Console.WriteLine("---------------------------------------------------");

			//Printing data
			Console.WriteLine("First name: {0}", arr[0].FirstName);
			Console.WriteLine("Last name: {0}", arr[0].LastName);
			Console.WriteLine("Birthdate: {0}", arr[0].BirthDate);
			Console.WriteLine("Address Line: {0}", arr[0].AddressLine1);
			Console.WriteLine("Postcode: {0}", arr[0].PostCode);
			Console.WriteLine("Country: {0}", arr[0].Country);
		}
	}
}